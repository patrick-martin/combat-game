//Creating constructor function -> Person
function Person(name, hp) {
    this.name = name,
        this.hp = hp,
        this.attack1 = function() {
            console.log(`${this.name} has been ATTACKED and damaged -20hp!`)
            this.hp -= 20;
        },
        this.attack2 = function() {
            console.log(`${this.name} has been ATTACKED and damaged -10hp!`)
            this.hp -= 10;
        },
        this.heal = function() {
            console.log(`${this.name} has HEALED himself and added +10hp!`)
            this.hp += 10;
        }
}

//Create our character objects from our Person constructor -> Mage(player1), Paladin(player2)
const theMage = new Person('Atticus the Mage', 50)
const thePaladin = new Person('Rynard the Paladin', 50)


const methods = ['attack1', 'attack2', 'heal'] //click-event will target one of these methods
const output = document.getElementById('output') //where we want to output our battle -->also could have used document.getElementById('#battle-output')[0]
const mage = document.getElementById('mage')
const paladin = document.getElementById('paladin')
const mageHP = document.getElementById('mage-hp')
const paladinHP = document.getElementById('paladin-hp')
let player = 1;

function randomNum() {
    return Math.floor(Math.random() * 3)
}

function startCombat(event) { //event = click;  event.target.id = element(image).id
    console.log('Start Combat is running')
    if (player === 1 && event.target.id === 'mage') {
        let action = methods[randomNum()];
        console.log(action)
        if (action === 'attack1') {
            output.innerHTML = `${theMage.name} has been struck by a sword!  -20HP`;
            theMage.hp -= 20;
            mageHP.innerHTML = theMage.hp;
        }
        if (action === 'attack2') {
            output.innerHTML = `${theMage.name} has been struck by a shield!  -10HP`;
            theMage.hp -= 10;
            mageHP.innerHTML = theMage.hp;
        }
        if (action === 'heal') {
            output.innerHTML = `${Mage.name} has cast a healing spell.  +10HP`;
            theMage.hp += 10;
            mageHP.innerHTML = theMage.hp;
        }

        checkWin();
        player += 1; //Should switch player to player 2

    }

    if (player === 2 && event.target.id === 'paladin') {
        let action = methods[randomNum()];
        console.log(action)
        if (action === 'attack1') {
            output.innerHTML = `${thePaladin.name} has been hit by a fireball!  -20HP`;
            thePaladin.hp -= 20;
            paladinHP.innerHTML = thePaladin.hp;
        }
        if (action === 'attack2') {
            output.innerHTML = `${thePaladin.name} has been hit by a lightning bolt!  -10HP`;
            thePaladin.hp -= 10;
            paladinHP.innerHTML = thePaladin.hp;
        }
        if (action === 'heal') {
            output.innerHTML = `${thePaladin.name} has applied a bandage.  +10HP`;
            thePaladin.hp += 10;
            paladinHP.innerHTML = thePaladin.hp;
        }
        checkWin();
        player -= 1;
    } else {
        alert('It is not your turn.  Please click the other combatant.')
    }
}

//Now we add our 'click event' and pass in the startCombat function as the callback

mage.addEventListener('click', startCombat)
paladin.addEventListener('click', startCombat)

function checkWin() {
    if (thePaladin.hp <= 0 || theMage.hp <= 0) {
        alert('You have been slain!');
        thePaladin.hp = 50;
        theMage.hp = 50;
        mageHP.innerHTML = theMage.hp;
        paladinHP.innerHTML = thePaladin.hp;
        output.innerHTML = 'Next Round.   FIGHT!'
    }
}

//Could include additional 'scoreboard' to keep track of each win
//would include this to the left & right of the HP-count scoreboard
//Also could include names of the characters above their images.  
//Or could allow the player to select which character he would prefer to be, by creating a prompt that the User will select either 1 or 2
//1 being the Mage, 2 being the Paladin